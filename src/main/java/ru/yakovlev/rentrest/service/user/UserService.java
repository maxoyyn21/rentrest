package ru.yakovlev.rentrest.service.user;

import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.enums.TelegramAction;
import ru.yakovlev.rentrest.model.enums.TransportTypeEnum;

public interface UserService {
    AppUser findByTelegramUsername(String telegramUsername);
    AppUser findById(Long id);
    AppUser save(AppUser user);
    boolean isPresent(AppUser user);
    AppUser updateTelegramParameters(String telegramUsername, TelegramAction action, TransportTypeEnum telegramTransportType, String transportName);
}
