package ru.yakovlev.rentrest.service.rent;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yakovlev.rentrest.access.repository.RentRepository;
import ru.yakovlev.rentrest.exception.AccessRuntimeException;
import ru.yakovlev.rentrest.exception.BusinessRuntimeException;
import ru.yakovlev.rentrest.model.context.UserContext;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.entity.Parking;
import ru.yakovlev.rentrest.model.entity.Rent;
import ru.yakovlev.rentrest.model.entity.Transport;
import ru.yakovlev.rentrest.model.enums.ParkingTypeEnum;
import ru.yakovlev.rentrest.model.enums.RentStatusEnum;
import ru.yakovlev.rentrest.model.enums.TransportRentStatusEnum;
import ru.yakovlev.rentrest.model.enums.TransportTypeEnum;
import ru.yakovlev.rentrest.security.SecurityContext;
import ru.yakovlev.rentrest.service.parking.ParkingService;
import ru.yakovlev.rentrest.service.transport.TransportService;
import ru.yakovlev.rentrest.service.user.UserService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RentServiceImpl implements RentService{
    private final RentRepository rentRepository;
    private final UserService userService;
    private final TransportService transportService;
    private final ParkingService parkingService;

    @Value("${settings.rent.maxCount}")
    private int maxCountRent;

    @Value("${settings.tariff.electricScooter.base}")
    private BigDecimal electricScooterBaseCost;
    @Value("${settings.tariff.electricScooter.costPerMinute}")
    private BigDecimal electricScooterCostPerMinute;

    @Value("${settings.tariff.bicycle.base}")
    private BigDecimal bicycleBaseCost;
    @Value("${settings.tariff.bicycle.costPerMinute}")
    private BigDecimal bicycleCostPerMinute;

    @Override
    @Transactional
    public synchronized Rent createRent(AppUser user, Long transportId) {
        Rent rent = new Rent();

        SecurityContext.set(new UserContext(user.getId(), user.getRole()));

        if(findUserRents(RentStatusEnum.OPEN).size() == maxCountRent){
            throw new BusinessRuntimeException("Нельзя открыть больше " + maxCountRent + " аренд", HttpStatus.BAD_REQUEST);
        }

        Transport transport = transportService.findById(transportId);

        if(transport.getType() == TransportTypeEnum.ELECTRIC_SCOOTER && transport.getCharge() <= 10){
            throw new BusinessRuntimeException("Слишком низкий процент заряда", HttpStatus.BAD_REQUEST);
        }

        if(transport.getStatus() == TransportRentStatusEnum.BUSY || transport.getStatus() == TransportRentStatusEnum.DELETED){
            throw new BusinessRuntimeException("Транспорт сейчас не доступен", HttpStatus.BAD_REQUEST);
        }

        if((transport.getType() == TransportTypeEnum.ELECTRIC_SCOOTER && user.getAmount()
                .subtract(electricScooterBaseCost)
                .compareTo(BigDecimal.ZERO) < 0)
                ||
                (transport.getType() == TransportTypeEnum.BICYCLE && user.getAmount()
                        .subtract(bicycleBaseCost)
                        .compareTo(BigDecimal.ZERO) < 0)
        ){
            throw new BusinessRuntimeException("Не хватает денег", HttpStatus.BAD_REQUEST);
        }

        LocalDateTime now = LocalDateTime.now();

        rent.setStartRentDatetime(now);
        rent.setStatus(RentStatusEnum.OPEN);
        rent.setTransport(transport);
        rent.setUser(user);
        rent.setStartParking(transport.getParking());

        Transport updatedTransport = transportService.updateRentStatus(transport, TransportRentStatusEnum.BUSY);

        if(updatedTransport == null){
            throw new AccessRuntimeException("Транспорт не арендован", HttpStatus.NOT_MODIFIED);
        }

        SecurityContext.clear();

        return rentRepository.save(rent);
    }

    @Override
    public Rent findById(Long id) {
        return rentRepository.findById(id).orElseThrow(() -> new AccessRuntimeException("Запись о аренде не найдена", HttpStatus.NOT_FOUND));
    }

    @Override
    @Transactional
    public synchronized Rent closeRent(AppUser user, Long rentId, Double lat, Double lon) {
        Rent rent = findById(rentId);
        Parking parking = parkingService.findParkingByCoordinates(lat, lon).orElseThrow(() -> new BusinessRuntimeException("Вы не в зоне парковки", HttpStatus.BAD_REQUEST));

        if(!rent.getUser().getId().equals(user.getId())){
            throw new BusinessRuntimeException("Нельзя закрыть чужую аренду", HttpStatus.BAD_REQUEST);
        }

        if(rent.getStatus() == RentStatusEnum.CLOSE){
            throw new BusinessRuntimeException("Аренда уже закрыта", HttpStatus.BAD_REQUEST);
        }

        if((parking.getType() == ParkingTypeEnum.BICYCLE && rent.getTransport().getType() == TransportTypeEnum.ELECTRIC_SCOOTER)
                || (parking.getType() == ParkingTypeEnum.ELECTRIC_SCOOTER && rent.getTransport().getType() == TransportTypeEnum.BICYCLE)
        ){
            throw new BusinessRuntimeException("Парковка не соответствует типу транспорта", HttpStatus.BAD_REQUEST);
        }

        LocalDateTime now = LocalDateTime.now();

        Long minutes = ChronoUnit.MINUTES.between(rent.getStartRentDatetime(), now);
        BigDecimal fullCost = null;

        if(rent.getTransport().getType() == TransportTypeEnum.ELECTRIC_SCOOTER){
            fullCost = electricScooterBaseCost.add(electricScooterCostPerMinute.multiply(BigDecimal.valueOf(minutes)));
        }

        if(rent.getTransport().getType() == TransportTypeEnum.BICYCLE){
            fullCost = bicycleBaseCost.add(bicycleCostPerMinute.multiply(BigDecimal.valueOf(minutes)));
        }

        BigDecimal remainingMoney = rent.getUser().getAmount().subtract(fullCost);

        if(remainingMoney.compareTo(BigDecimal.ZERO) < 0){
            throw new BusinessRuntimeException("Не хватает денег", HttpStatus.BAD_REQUEST);
        }

        user.setAmount(remainingMoney);
        AppUser updatedUser = userService.save(user);

        if(updatedUser == null){
            throw new BusinessRuntimeException("Счет не обновлен", HttpStatus.NOT_MODIFIED);
        }

        Transport transport = transportService.findById(rent.getTransport().getId());
        transport.setStatus(TransportRentStatusEnum.FREE);
        transport.setParking(parking);
        transport.setLatitude(lat.toString());
        transport.setLongitude(lon.toString());
        Transport updatedTransport = transportService.save(transport);

        if(updatedTransport == null){
            throw new BusinessRuntimeException("Транспорт все еще в аренде", HttpStatus.NOT_MODIFIED);
        }

        rent.setStatus(RentStatusEnum.CLOSE);
        rent.setAmount(fullCost);
        rent.setEndRentDatetime(now);
        rent.setEndParking(parking);

        return rentRepository.save(rent);
    }

    @Override
    public List<Rent> findUserRents(RentStatusEnum rentStatus) {
        AppUser user = userService.findById(SecurityContext.get().getId());

        return findRents(user, rentStatus);
    }

    @Override
    public List<Rent> findTelegramUserRents(String telegramUsername, RentStatusEnum rentStatus) {
        AppUser user = userService.findByTelegramUsername(telegramUsername);

        return findRents(user, rentStatus);
    }

    private List<Rent> findRents(AppUser user, RentStatusEnum rentStatus){
        Rent rent = new Rent();

        rent.setUser(user);

        if(rentStatus != null){
            rent.setStatus(rentStatus);
        }

        Example<Rent> rentExample = Example.of(rent);

        return rentRepository.findAll(rentExample);
    }

    @Override
    public Page<Rent> findAll(RentStatusEnum status, Long userId, Long transportId, int page, int size) {
        Rent rent = new Rent();

        AppUser user = null;
        Transport transport = null;

        if(userId != null){
            user = userService.findById(userId);
            rent.setUser(user);
        }

        if(transportId != null){
            transport = transportService.findById(transportId);
            rent.setTransport(transport);
        }

        if(status != null){
            rent.setStatus(status);
        }

        Example<Rent> rentExample = Example.of(rent);

        Pageable pageRequest = PageRequest.of(page, size);

        return rentRepository.findAll(rentExample, pageRequest);
    }

    @Override
    public Optional<Rent> findOpenByUserAndTransportName(AppUser user, String transportName) {
        Rent rent = new Rent();
        rent.setUser(user);
        rent.setStatus(RentStatusEnum.OPEN);

        Transport transport = new Transport();
        transport.setName(transportName);

        rent.setTransport(transport);

        Example<Rent> rentExample = Example.of(rent);

        return rentRepository.findOne(rentExample);
    }
}
