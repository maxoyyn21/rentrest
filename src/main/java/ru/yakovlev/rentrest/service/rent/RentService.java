package ru.yakovlev.rentrest.service.rent;

import org.springframework.data.domain.Page;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.entity.Rent;
import ru.yakovlev.rentrest.model.enums.RentStatusEnum;

import java.util.List;
import java.util.Optional;

public interface RentService {
    Rent createRent(AppUser user, Long transportId);
    Rent findById(Long id);
    Rent closeRent(AppUser user, Long rentId, Double lat, Double lon);
    List<Rent> findUserRents(RentStatusEnum rentStatus);
    List<Rent> findTelegramUserRents(String telegramUsername, RentStatusEnum rentStatus);
    Page<Rent> findAll(RentStatusEnum status, Long userId, Long transportId, int page, int size);
    Optional<Rent> findOpenByUserAndTransportName(AppUser user, String transportName);
}
