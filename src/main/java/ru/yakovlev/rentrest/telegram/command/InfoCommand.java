package ru.yakovlev.rentrest.telegram.command;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.DefaultBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.yakovlev.rentrest.exception.AccessRuntimeException;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.entity.Rent;
import ru.yakovlev.rentrest.model.enums.RentStatusEnum;
import ru.yakovlev.rentrest.service.rent.RentService;
import ru.yakovlev.rentrest.service.user.UserService;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

@Slf4j
public class InfoCommand extends DefaultBotCommand {
    private RentService rentService;
    private UserService userService;

    public InfoCommand(String commandIdentifier, String description, RentService rentService, UserService userService) {
        super(commandIdentifier, description);

        this.rentService = rentService;
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Integer integer, String[] strings) {
        SendMessage sendMessage = new SendMessage();

        sendMessage.setParseMode("html");
        sendMessage.setChatId(chat.getId().toString());

        try{
            AppUser appUser = userService.findByTelegramUsername(user.getUserName());
            List<Rent> rents = rentService.findTelegramUserRents(user.getUserName(), RentStatusEnum.OPEN);
            String info = getUserInfo(rents, appUser);

            sendMessage.setText(info);

            absSender.execute(sendMessage);
        }
        catch (AccessRuntimeException exception){
            log.error(exception.getMessage());
            sendMessage.setText("Зарегистрируйтесь /help");
            absSender.execute(sendMessage);
        }
    }

    private String getUserInfo(List<Rent> rents, AppUser user){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(String.format("Баланс: %s руб.\n", user.getAmount().toPlainString()));

        if(rents.size() == 0){
            stringBuilder.append("Нет открытых аренд");
        }
        else {
            stringBuilder.append(String.format("Открыто аренд: %d\n\n", rents.size()));

            for(var rent: rents){
                stringBuilder.append(String.format("Начало аренды: %s\n", DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(rent.getStartRentDatetime())));
                stringBuilder.append(String.format("Транспорт: %s\n\n", rent.getTransport().getName()));
            }
        }

        return stringBuilder.toString().trim();
    }
}
