package ru.yakovlev.rentrest.telegram.command;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.DefaultBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.yakovlev.rentrest.exception.AccessRuntimeException;
import ru.yakovlev.rentrest.exception.BusinessRuntimeException;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.entity.Rent;
import ru.yakovlev.rentrest.model.enums.TransportRentStatusEnum;
import ru.yakovlev.rentrest.service.rent.RentService;
import ru.yakovlev.rentrest.service.transport.TransportService;
import ru.yakovlev.rentrest.service.user.UserService;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

@Slf4j
public class RentCommand extends DefaultBotCommand {
    private UserService userService;
    private TransportService transportService;
    private RentService rentService;

    public RentCommand(String commandIdentifier, String description, UserService userService, TransportService transportService, RentService rentService) {
        super(commandIdentifier, description);

        this.userService = userService;
        this.transportService = transportService;
        this.rentService = rentService;
    }

    @SneakyThrows
    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Integer integer, String[] strings) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chat.getId().toString());

        try {
            AppUser appUser = userService.findByTelegramUsername(user.getUserName());

            if(strings.length != 1){
                sendMessage.setText("Неправильная команда /help");
            }
            else{
                String transportName = strings[0].toUpperCase(Locale.ROOT);

                var transport = transportService.findByNameAndStatus(transportName, TransportRentStatusEnum.FREE);

                if(transport.isEmpty()){
                    sendMessage.setText("Нет такого транспорта или он занят");
                }
                else {
                    Rent rent = rentService.createRent(appUser, transport.get().getId());
                    
                    if(rent == null){
                        sendMessage.setText("Аренда не прошла");
                    }
                    else{
                        sendMessage.setText(
                                String.format(
                                        "Вы арендовали %s\nНачало аренды %s\nПарковка %s",
                                        rent.getTransport().getName(),
                                        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(rent.getStartRentDatetime()),
                                        rent.getTransport().getParking().getName()
                                )
                        );
                    }
                }
            }
        }
        catch (AccessRuntimeException exception){
            log.error(exception.getMessage());

            if(exception.getHttpStatus() == HttpStatus.NOT_FOUND){
                sendMessage.setText("Зарегистрируйтесь /help");
            }
            else{
                sendMessage.setText(exception.getMessage());
            }
        }
        catch (BusinessRuntimeException exception){
            log.error(exception.getMessage());
            sendMessage.setText(exception.getMessage());
        }
        finally {
            absSender.execute(sendMessage);
        }
    }
}
