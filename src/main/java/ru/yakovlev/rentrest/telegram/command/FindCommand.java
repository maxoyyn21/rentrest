package ru.yakovlev.rentrest.telegram.command;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.DefaultBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.yakovlev.rentrest.exception.AccessRuntimeException;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.enums.TelegramAction;
import ru.yakovlev.rentrest.model.enums.TransportTypeEnum;
import ru.yakovlev.rentrest.service.user.UserService;
import ru.yakovlev.rentrest.utils.TelegramHelper;

import java.util.Locale;

@Slf4j
public class FindCommand extends DefaultBotCommand {
    private UserService userService;

    public FindCommand(String commandIdentifier, String description, UserService userService) {
        super(commandIdentifier, description);

        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public synchronized void execute(AbsSender absSender, User user, Chat chat, Integer integer, String[] strings) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chat.getId().toString());

        try{
            TransportTypeEnum transportType = null;

            if(strings.length > 0) {
                String transport = strings[0].toLowerCase(Locale.ROOT);

                if (transport.equals("эсм")) {
                    transportType = TransportTypeEnum.ELECTRIC_SCOOTER;
                } else if (transport.equals("вел")) {
                    transportType = TransportTypeEnum.BICYCLE;
                } else {
                    sendMessage.setParseMode("html");
                    sendMessage.setText("Нет такого типа транспорта /help");
                    absSender.execute(sendMessage);
                    return;
                }
            }

            AppUser updatedUser = userService.updateTelegramParameters(user.getUserName(), TelegramAction.FIND, transportType, null);

            if(updatedUser == null){
                sendMessage.setText("Произошла ошибка, попробуйте позже");
                absSender.execute(sendMessage);
            }
            else{
                sendMessage.setText("Ваша геолокация?");
                sendMessage.setReplyMarkup(TelegramHelper.getGeolocationButtonKeyboardMarkup());

                absSender.execute(sendMessage);
            }
        }
        catch (TelegramApiException exception){
            log.error(exception.getMessage());
        }
        catch (AccessRuntimeException exception){
            sendMessage.setText("Зарегистрируйтесь /help");
            absSender.execute(sendMessage);
        }
    }
}
