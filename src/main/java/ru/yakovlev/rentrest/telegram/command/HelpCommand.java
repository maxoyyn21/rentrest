package ru.yakovlev.rentrest.telegram.command;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.DefaultBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Slf4j
public class HelpCommand extends DefaultBotCommand {
    public HelpCommand(@NonNull String command, @NonNull String description) {
        super(command, description);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Integer integer, String[] strings) {
        try{
            SendMessage sendMessage = new SendMessage();
            sendMessage.setParseMode("html");
            sendMessage.setChatId(chat.getId().toString());
            sendMessage.setText(getHelpText());

            absSender.execute(sendMessage);
        }
        catch (Exception exception){
            log.error(exception.getMessage());
        }
    }

    public String getHelpText(){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("/start email - регистрация\n");
        stringBuilder.append("/info - информация о текущем пользователе\n");
        stringBuilder.append("/find [эсм|вел] - поиск ТС опционально по типу\n");
        stringBuilder.append("/rent [название ТС] - аренда ТС\n");
        stringBuilder.append("/close [название ТС] - закрыть аренду ТС\n");

        return stringBuilder.toString();
    }
}
