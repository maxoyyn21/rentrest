package ru.yakovlev.rentrest.telegram.command;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.DefaultBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import ru.yakovlev.rentrest.exception.AccessRuntimeException;
import ru.yakovlev.rentrest.exception.BusinessRuntimeException;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.enums.TelegramAction;
import ru.yakovlev.rentrest.service.rent.RentService;
import ru.yakovlev.rentrest.service.transport.TransportService;
import ru.yakovlev.rentrest.service.user.UserService;
import ru.yakovlev.rentrest.utils.TelegramHelper;

import java.util.Locale;

@Slf4j
public class CloseCommand extends DefaultBotCommand {
    private UserService userService;
    private RentService rentService;
    private TransportService transportService;

    public CloseCommand(String commandIdentifier, String description, UserService userService, RentService rentService, TransportService transportService) {
        super(commandIdentifier, description);

        this.userService = userService;
        this.rentService = rentService;
        this.transportService = transportService;
    }

    @SneakyThrows
    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Integer integer, String[] strings) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chat.getId().toString());

        try{
            AppUser appUser = userService.findByTelegramUsername(user.getUserName());

            if(strings.length != 1){
                sendMessage.setText("Неправильная команда /help");
            }
            else{
                String transportName = strings[0];

                if(transportService.findByNameAndStatus(transportName.toUpperCase(Locale.ROOT), null).isEmpty()){
                    sendMessage.setText(String.format("Нет транспорта %s", transportName));
                }
                else if(rentService.findOpenByUserAndTransportName(appUser, transportName.toUpperCase(Locale.ROOT)).isEmpty()){
                    sendMessage.setText(String.format("У вас нет открытой аренды транспорта %s", transportName));
                }
                else{
                    AppUser updatedUser = userService.updateTelegramParameters(user.getUserName(), TelegramAction.CLOSE, null, transportName.toUpperCase(Locale.ROOT));

                    if(updatedUser == null){
                        sendMessage.setText("Произошла ошибка, попробуйте позже");
                        absSender.execute(sendMessage);
                    }
                    else{
                        sendMessage.setText("Ваша геолокация?");
                        sendMessage.setReplyMarkup(TelegramHelper.getGeolocationButtonKeyboardMarkup());
                    }
                }
            }
        }
        catch (AccessRuntimeException exception){
            log.error(exception.getMessage());

            if(exception.getHttpStatus() == HttpStatus.NOT_FOUND){
                sendMessage.setText("Зарегистрируйтесь /help");
            }
            else{
                sendMessage.setText(exception.getMessage());
            }
        }
        catch (BusinessRuntimeException exception){
            log.error(exception.getMessage());
            sendMessage.setText(exception.getMessage());
        }
        finally {
            absSender.execute(sendMessage);
        }
    }
}
