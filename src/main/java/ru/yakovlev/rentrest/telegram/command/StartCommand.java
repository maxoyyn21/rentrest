package ru.yakovlev.rentrest.telegram.command;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.DefaultBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.enums.UserRoleEnum;
import ru.yakovlev.rentrest.service.user.UserService;

import java.math.BigDecimal;
import java.util.UUID;

@Slf4j
public class StartCommand extends DefaultBotCommand {
    private UserService userService;

    public StartCommand(String commandIdentifier, String description, UserService userService) {
        super(commandIdentifier, description);

        this.userService = userService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Integer integer, String[] strings) {
        try {
            SendMessage sendMessage = new SendMessage();

            sendMessage.setParseMode("html");
            sendMessage.setChatId(chat.getId().toString());

            if(strings.length != 1 || !strings[0].contains("@")){
                sendMessage.setText("Нужно передать email");
            }
            else{
                String email = strings[0];

                AppUser appUserWithEmail = new AppUser();
                appUserWithEmail.setEmail(email);

                AppUser appUserWithTelegramUsername = new AppUser();
                appUserWithTelegramUsername.setTelegramUsername(user.getUserName());

                if(userService.isPresent(appUserWithTelegramUsername)){
                    sendMessage.setText(String.format("Пользователь %s уже зарегистрирован", user.getUserName()));
                }
                else if(userService.isPresent(appUserWithEmail)){
                    sendMessage.setText(String.format("Email %s уже зарегистрирован", email));
                }
                else{
                    String telegramUsername = user.getUserName();
                    String password = UUID.randomUUID().toString().substring(0, 6);

                    AppUser appUser = new AppUser();

                    appUser.setAmount(BigDecimal.valueOf(100000.00));
                    appUser.setRole(UserRoleEnum.USER);
                    appUser.setTelegramUsername(telegramUsername);
                    appUser.setEmail(email);
                    appUser.setPassword(password);

                    AppUser savedUser = userService.save(appUser);

                    if(savedUser == null){
                        sendMessage.setText("Ошибка регистрации, попробуйте позже");
                    }
                    else{
                        sendMessage.setText(String.format("Вы зарегистрировались!\nEmail: %s\nПароль: %s", email, password));
                    }
                }
            }

            absSender.execute(sendMessage);
        } catch (TelegramApiException exception){
            log.error(exception.getMessage());
        }
    }
}
