package ru.yakovlev.rentrest.telegram;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.yakovlev.rentrest.exception.BusinessRuntimeException;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.entity.Rent;
import ru.yakovlev.rentrest.model.entity.Transport;
import ru.yakovlev.rentrest.model.enums.TelegramAction;
import ru.yakovlev.rentrest.model.mapping.RentMapper;
import ru.yakovlev.rentrest.service.parking.ParkingService;
import ru.yakovlev.rentrest.service.rent.RentService;
import ru.yakovlev.rentrest.service.transport.TransportService;
import ru.yakovlev.rentrest.service.user.UserService;
import ru.yakovlev.rentrest.telegram.command.*;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

@Component
@Slf4j
public class TelegramBot extends TelegramLongPollingCommandBot {
    @Value("${telegram.bot.name}")
    private String botName;

    @Value("${telegram.bot.token}")
    private String botToken;

    private UserService userService;
    private RentService rentService;
    private TransportService transportService;
    private ParkingService parkingService;

    public TelegramBot(UserService userService, RentService rentService, RentMapper rentMapper, TransportService transportService, ParkingService parkingService) {
        this.userService = userService;
        this.rentService = rentService;
        this.transportService = transportService;
        this.parkingService = parkingService;

        this.register(new HelpCommand("help", "Выводит информация о доступных командах"));
        this.register(new StartCommand("start", "Регистрация пользователя", userService));
        this.register(new InfoCommand("info", "Информация о текущем пользователе", rentService, userService));
        this.register(new FindCommand("find", "Поиск транспорта", userService));
        this.register(new RentCommand("rent", "Аренда транспорта", userService, transportService, rentService));
        this.register(new CloseCommand("close", "Закрытие аренды ТС", userService, rentService, transportService));
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @SneakyThrows
    @Override
    public void processNonCommandUpdate(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setParseMode("html");
        sendMessage.setChatId(update.getMessage().getChatId().toString());

        try{
            if(update.getMessage().getLocation() != null){
                String telegramUsername = update.getMessage().getChat().getUserName();
                AppUser appUser = userService.findByTelegramUsername(telegramUsername);

                if(appUser.getTelegramAction() == TelegramAction.FIND){
                    List<Transport> transports = transportService.findByCoordinatesAndType(
                            update.getMessage().getLocation().getLatitude(),
                            update.getMessage().getLocation().getLongitude(),
                            appUser.getTelegramTransportType()
                    );

                    StringBuilder stringBuilder = new StringBuilder();

                    for(Transport transport: transports){
                        stringBuilder.append(String.format("%s - %.0f м. - %s\n", transport.getName(), transport.getDistanceToUser(), transport.getParking().getName()));
                    }

                    sendMessage.setText(stringBuilder.toString());

                    userService.updateTelegramParameters(telegramUsername, null, null, null);
                }
                else if(appUser.getTelegramAction() == TelegramAction.CLOSE){
                    Double lat = update.getMessage().getLocation().getLatitude();
                    Double lon = update.getMessage().getLocation().getLongitude();

                    var parking = parkingService.findParkingByCoordinates(lat, lon);

                    if(parking.isPresent()) {
                        Rent rent = rentService.findOpenByUserAndTransportName(appUser, appUser.getTelegramTransportName())
                                .orElseThrow(() -> new BusinessRuntimeException(
                                        String.format("У вас нет открытой аренды транспорта %s", appUser.getTelegramTransportName()), HttpStatus.BAD_REQUEST));

                        Rent closedRent = rentService.closeRent(appUser, rent.getId(), lat, lon);

                        if (closedRent != null) {
                            sendMessage.setText(printCloseRentInfo(closedRent));

                            userService.updateTelegramParameters(telegramUsername, null, null, null);
                        } else {
                            throw new BusinessRuntimeException("Аренда не закрыта", HttpStatus.BAD_REQUEST);
                        }
                    }
                    else{
                        throw new BusinessRuntimeException("Вы не в зоне парковки", HttpStatus.BAD_REQUEST);
                    }
                }
                else{
                    sendMessage.setText("Используйте команду /help");
                }
            }
            else{
                sendMessage.setText("Используйте команду /help");
            }

            execute(sendMessage);
        }
        catch (TelegramApiException exception){
            log.error(exception.getMessage());
        }
        catch (BusinessRuntimeException exception){
            sendMessage.setText(exception.getMessage());
            execute(sendMessage);

            AppUser user = userService.findByTelegramUsername(update.getMessage().getChat().getUserName());
            if(user.getTelegramAction() != null || (user.getTelegramTransportType() != null && user.getTelegramTransportName() != null)){
                userService.updateTelegramParameters(update.getMessage().getChat().getUserName(), null, null, null);
            }
        }
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    private String printCloseRentInfo(Rent rent){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(String.format("Аренда %s закрыта\n", rent.getTransport().getName()));
        stringBuilder.append(String.format("Стоимость: %.2f руб.\n", rent.getAmount()));
        stringBuilder.append(String.format("Начало аренды: %s\n", DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(rent.getStartRentDatetime())));
        stringBuilder.append(String.format("Конец аренды: %s", DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(rent.getEndRentDatetime())));

        return stringBuilder.toString();
    }
}
