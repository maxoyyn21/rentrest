package ru.yakovlev.rentrest.model.enums;

public enum TransportTypeEnum {
    ELECTRIC_SCOOTER,
    BICYCLE
}
