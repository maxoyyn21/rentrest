package ru.yakovlev.rentrest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.yakovlev.rentrest.exception.AccessRuntimeException;
import ru.yakovlev.rentrest.exception.BusinessRuntimeException;
import ru.yakovlev.rentrest.model.dto.rent.CloseRentDto;
import ru.yakovlev.rentrest.model.dto.rent.CreateRentDto;
import ru.yakovlev.rentrest.model.dto.rent.RentDto;
import ru.yakovlev.rentrest.model.dto.rent.ResponseRentPageDto;
import ru.yakovlev.rentrest.model.entity.AppUser;
import ru.yakovlev.rentrest.model.entity.Rent;
import ru.yakovlev.rentrest.model.enums.RentStatusEnum;
import ru.yakovlev.rentrest.model.mapping.RentMapper;
import ru.yakovlev.rentrest.security.SecurityContext;
import ru.yakovlev.rentrest.service.rent.RentService;
import ru.yakovlev.rentrest.service.user.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rents")
@RequiredArgsConstructor
public class RentController {
    private final RentService rentService;
    private final UserService userService;
    private final RentMapper rentMapper;

    @PatchMapping("/{id}/close")
    public RentDto closeRent(@PathVariable Long id, @Valid @RequestBody CloseRentDto closeRentDto){
        AppUser user = userService.findById(SecurityContext.get().getId());

        Rent closedRent = rentService.closeRent(
                user,
                id,
                closeRentDto.getEndRentLatitude(),
                closeRentDto.getEndRentLongitude()
        );

        if(closedRent == null){
            throw new BusinessRuntimeException("Аренда не закрыта", HttpStatus.NOT_MODIFIED);
        }

        return rentMapper.modelToDto(closedRent);
    }

    @PostMapping
    public RentDto createRent(@Valid @RequestBody CreateRentDto createRentDto){
        AppUser user = userService.findById(SecurityContext.get().getId());

        Rent savedRent = rentService.createRent(user, createRentDto.getTransportId());

        if(savedRent == null){
            throw new AccessRuntimeException("Аренда не прошла", HttpStatus.BAD_REQUEST);
        }

        return rentMapper.modelToDto(savedRent);
    }

    @GetMapping("/current-user")
    public List<RentDto> findUserRents(@RequestParam(required = false) RentStatusEnum status){
        List<Rent> userRents = rentService.findUserRents(status);

        return rentMapper.modelToDto(userRents);
    }

    @GetMapping
    public ResponseRentPageDto findAll(
            @RequestParam(required = false) RentStatusEnum status,
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) Long transportId,
            @RequestParam int page,
            @RequestParam int size
    ){
        Page<Rent> rentPage = rentService.findAll(status, userId, transportId, page, size);

        return new ResponseRentPageDto(
                rentPage.getTotalElements(),
                rentPage.getTotalPages(),
                rentPage.getSize(),
                rentPage.getNumberOfElements(),
                rentMapper.modelToDto(rentPage.getContent())
        );
    }
}
